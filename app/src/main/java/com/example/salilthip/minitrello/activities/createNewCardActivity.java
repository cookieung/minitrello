package com.example.salilthip.minitrello.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


import com.example.salilthip.minitrello.R;
import com.example.salilthip.minitrello.models.Card;
import com.example.salilthip.minitrello.utils.Storage;

public class createNewCardActivity extends AppCompatActivity {

    private EditText tileText;
    private EditText description;
    private Card card;
    private Button saveButton,cancelButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_new_card);
        initComponents();
    }

    public void initComponents(){
        tileText = (EditText) findViewById(R.id.title_text);
        description = (EditText) findViewById(R.id.description_text);
        cancelButton = (Button) findViewById(R.id.cancel_card_btn);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        saveButton = (Button) findViewById(R.id.save_this_card);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(tileText.getText().length()!=0&&description.getText().length()!=0){
                    card = new Card(tileText.getText().toString(), description.getText().toString());
                    Intent intent = getIntent();
                    int i = (int) intent.getSerializableExtra("location");
                    Storage.getInstance().getSaveList().get(i).addCard(card);
                    finish();
                }
                else {
                    if (tileText.getText().length() == 0) {
                        tileText.setHint("Please fill the title!!");
                    }
                    if (description.getText().length() == 0) {
                        description.setHint("Please fill the description!!");
                    }
                }
            }
        });

    }
}
