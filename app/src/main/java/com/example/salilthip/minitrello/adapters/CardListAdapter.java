package com.example.salilthip.minitrello.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.salilthip.minitrello.R;
import com.example.salilthip.minitrello.models.CardList;

import java.util.List;

/**
 * Created by Salilthip on 3/5/2016.
 */
public class CardListAdapter extends ArrayAdapter<CardList> {
    public CardListAdapter(Context context, int resource,List<CardList> cl) {
        super(context, resource,cl);

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if(v==null){
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.cardlist_cell, null);
        }

        TextView name = (TextView) v.findViewById(R.id.name_card_list);
        TextView id = (TextView) v.findViewById(R.id.id_card_list);

        name.setText("Name of list : "+getItem(position).getNameList());
        id.setText("ID : "+getItem(position).getId()+"");
        return v;
    }
}
