package com.example.salilthip.minitrello.utils;

import android.util.Log;

import com.example.salilthip.minitrello.models.Card;
import com.example.salilthip.minitrello.models.CardList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Salilthip on 3/2/2016.
 */
public class Storage {

    private static Storage instance;
    private List<CardList> saveList;

    private Storage(){
        initStorage();
    }

    public static Storage getInstance(){
        if(instance == null){ instance = new Storage(); }
        return instance;
    }

    private void initStorage() {
        CardList cardList1 = new CardList("cl1");
        cardList1.addCard(new Card("title3", "description1"));
        cardList1.addCard(new Card("title2", "description1"));
        cardList1.addCard(new Card("title1", "description1"));

        CardList cardList2 = new CardList("cl2");
        cardList2.addCard(new Card("title1", "description1"));
        cardList2.addCard(new Card("title2", "description1"));
        cardList2.addCard(new Card("title3", "description1"));

        CardList cardList3 = new CardList("cl3");
        cardList3.addCard(new Card("title1", "description1"));
        cardList3.addCard(new Card("title2", "description1"));
        cardList3.addCard(new Card("title3", "description1"));

        saveList = new ArrayList<CardList>();
        saveList.add(cardList2);
        saveList.add(cardList3);
        saveList.add(cardList1);
    }

    public void saveCardList(CardList cl){
        saveList.add(cl);
    }

    public List<CardList> getSaveList() { return getSaveList("ID"); }

    public List<CardList> getSaveList(String sortType){
        if(sortType.equalsIgnoreCase("ID")) {
            Collections.sort(saveList,CardList.IdComparator);
        } else if(sortType.equalsIgnoreCase("NAME")) {
            Collections.sort(saveList,CardList.NameComparator);
        }

        return saveList;
    }

    public int getNumberOfList(){
        return saveList.size();
    }

}
