package com.example.salilthip.minitrello.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.salilthip.minitrello.R;
import com.example.salilthip.minitrello.models.Card;
import com.example.salilthip.minitrello.models.CardList;
import com.example.salilthip.minitrello.utils.Storage;

import java.util.ArrayList;
import java.util.List;

public class CreateNewCardListActivity extends AppCompatActivity {

    private EditText listNameText;
    private Button saveButton;
    private Button cancelButton;

    private List<Card> cards;
    private CardList cardList;

      @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_new_card_list);
        cards = new ArrayList<Card>();

        initComponents();
    }

    public void initComponents(){

        listNameText = (EditText) findViewById(R.id.list_name_text);

        saveButton = (Button) findViewById(R.id.save_this_list);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(listNameText.getText().length()!=0) {
                    cardList = new CardList(listNameText.getText().toString());
                    Storage.getInstance().saveCardList(cardList);
                    finish();
                }
                else{
                    listNameText.setHint("Please fill the list name!!");
                }
            }
        });
        cancelButton = (Button) findViewById(R.id.cancel_btn);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               finish();
            }
        });


    }

}
