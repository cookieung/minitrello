package com.example.salilthip.minitrello.models;

import java.io.Serializable;

/**
 * Created by Noot on 13/3/2559.
 */
public class Comment implements Serializable {

    private static long id = 0;
    private String text;

    public Comment(String text){
        id++;
        this.text = text;
    }

    public String getText(){
        return text;
    }

    public long getId(){
        return this.id;
    }

    public void setText(String text){
        this.text = text;
    }
}
