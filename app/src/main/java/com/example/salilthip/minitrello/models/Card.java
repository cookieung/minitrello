package com.example.salilthip.minitrello.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;


/**
 * Created by Salilthip on 3/2/2016.
 */
public class Card implements Serializable {

    private static long cardId=0;
    private long id;
    private String title;
    private String description;
    private List<Comment> commentList;


    public Card(String title,String description){
        cardId++;
        this.id = cardId;
        this.title = title;
        this.description = description;
        commentList = new ArrayList<Comment>();
    }

    public static Comparator NameComparator = new Comparator() {
        public int compare(Object lt1, Object another) {
            String title1 = ((Card) lt1).getTitle();
            String title2 = ((Card) another).getTitle();
            return title1.compareTo(title2);
        }
    };

    public static Comparator IdComparator = new Comparator() {
        public int compare(Object lt1, Object another) {
            String id1 = ((Card) lt1).getId() + "";
            String id2 = ((Card) another).getId() + "";
            return id1.compareTo(id2);
        }
    };

    public void setTitle(String title){
        this.title = title;
    }

    public void setDescription(String description){
        this.description = description;
    }

    public long getId(){
        return this.id;
    }

    public String getTitle() { return  this.title; }

    public String getDescription() { return this.description; }

    public List<Comment> getCommentList(){
        return commentList;
    }

}
