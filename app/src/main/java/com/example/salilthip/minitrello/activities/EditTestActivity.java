package com.example.salilthip.minitrello.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.example.salilthip.minitrello.R;
import com.example.salilthip.minitrello.models.Card;
import com.example.salilthip.minitrello.utils.Storage;

public class EditTestActivity extends AppCompatActivity {


    private ListView cardInfo;
    private TextView nameListText;
    private TextView titleView;
    private TextView descriptionView;
    private EditText titleText;
    private EditText descriptionText;

    private Button backBtn;
    private Button editBtn;

    private Card card;
    private int idList,idCard;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_test);
        initComponents();
    }

    public void initComponents(){
        nameListText = (TextView) findViewById(R.id.head_card_edit_page);
        titleView = (TextView) findViewById(R.id.title_view);
        titleText = (EditText) findViewById(R.id.title_text);
        descriptionView = (TextView) findViewById(R.id.description_view);
        descriptionText = (EditText) findViewById(R.id.description_text);

        final Intent intent = getIntent();
        idList = (int)intent.getSerializableExtra("idList");
        idCard = (int)intent.getSerializableExtra("idCard");

        card = Storage.getInstance().getSaveList().get(idList).getAllCards().get(idCard);

        nameListText.setText(Storage.getInstance().getSaveList().get(idList).getNameList());
        titleText.setText(card.getTitle());
        descriptionText.setText(card.getDescription());



        backBtn = (Button) findViewById(R.id.back_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        editBtn = (Button) findViewById(R.id.edit_detail);
        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (titleText.getText().length() != 0 && descriptionText.getText().length() != 0) {
                    Storage.getInstance().getSaveList().get(idList).getAllCards().get(idCard).setTitle(titleText.getText().toString());
                    Storage.getInstance().getSaveList().get(idList).getAllCards().get(idCard).setDescription(descriptionText.getText().toString());
                    finish();
                }
            }
        });
    }

}
