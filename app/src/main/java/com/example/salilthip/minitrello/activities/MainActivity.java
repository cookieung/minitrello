package com.example.salilthip.minitrello.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;

import com.example.salilthip.minitrello.R;
import com.example.salilthip.minitrello.adapters.CardListAdapter;
import com.example.salilthip.minitrello.models.Card;
import com.example.salilthip.minitrello.models.CardList;
import com.example.salilthip.minitrello.utils.Storage;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ListView namelist;
    private CardListAdapter cardListAdapter;
    private List<CardList> lists;
    private Button create_list;
    private TextView initialText;
    private Button deleteAllBtn;
    private Switch switchBtn;
    private boolean checkSort;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();

    }

    public void init(){
        initialText = (TextView) findViewById(R.id.please_create_new_list);
        initialText.setVisibility(View.VISIBLE);
        lists = new ArrayList<CardList>();
        cardListAdapter = new CardListAdapter(this,R.layout.cardlist_cell,lists);
        namelist = (ListView) findViewById(R.id.name_list_view);
        namelist.setAdapter(cardListAdapter);

        final AlertDialog.Builder alertDeleteList = new AlertDialog.Builder(this);
        final AlertDialog.Builder alertDeleteAll = new AlertDialog.Builder(this);
        alertDeleteList.setTitle("Delete list");
        alertDeleteAll.setTitle("Delelte all card list");

        namelist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, CardListActivity.class);
                intent.putExtra("pos_cardlist", position);
                startActivity(intent);
            }
        });
        create_list = (Button) findViewById(R.id.create_new_list);
        create_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, CreateNewCardListActivity.class);
                startActivity(intent);
            }
        });

        deleteAllBtn = (Button) findViewById(R.id.deleteAllBtn);
        deleteAllBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                alertDeleteAll.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Storage.getInstance().getSaveList().clear();
                        initialText.setVisibility(View.VISIBLE);
                        onResume();
                        dialog.cancel();
                    }
                });

                alertDeleteAll.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

                AlertDialog alert1 = alertDeleteAll.create();
                alert1.show();
            }
        });

        switchBtn = (Switch) findViewById(R.id.switchBtn);
        switchBtn.setChecked(true);
        checkSort = switchBtn.isChecked();
        switchBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                checkSort = isChecked;
                onResume();
            }
        });




        namelist.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int pos, long id) {
                final int posfi = pos;
                alertDeleteList.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Storage.getInstance().getSaveList().remove(posfi);
                        onResume();
                        dialog.cancel();
                    }
                });

                alertDeleteList.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

                AlertDialog alert11 = alertDeleteList.create();
                alert11.show();
                return true;
            }

        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        loadCardList();
    }


    public void loadCardList(){
        lists.clear();
        String sortType;
        if(checkSort) {
            switchBtn.setText("Sort by: ID");
            sortType = "ID";
        }else{
            switchBtn.setText("Sort by: A-Z");
            sortType = "NAME";
        }

        lists.addAll(Storage.getInstance().getSaveList(sortType));
        cardListAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onStart(){
        super.onStart();
        loadCardList();
        if(Storage.getInstance().getSaveList().size()>0) {initialText.setVisibility(View.INVISIBLE);}
    }

}
