package com.example.salilthip.minitrello.models;

import android.util.Log;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Salilthip on 3/2/2016.
 */
public class CardList implements Serializable {

    private List<Card> cards;
    private static  long idList=0;
    private long id;
    private String nameList;

    public CardList(String nameList){
        ++idList;
        id = idList;
        cards = new ArrayList<Card>();
        this.nameList = nameList;
    }

    public static Comparator NameComparator = new Comparator() {
        public int compare(Object lt1, Object another) {
            String name1 = ((CardList) lt1).getNameList();
            String name2 = ((CardList) another).getNameList();
            return name1.compareTo(name2);
        }
    };

    public static Comparator IdComparator = new Comparator() {
        public int compare(Object lt1, Object another) {
            String id1 = ((CardList) lt1).getId() + "";
            String id2 = ((CardList) another).getId() + "";
            return id1.compareTo(id2);
        }
    };


    public void addCard(Card c){
        cards.add(c);
    }

    public List<Card> getAllCards(){
        return getAllCards("ID");
    }

    public List<Card> getAllCards(String sortType){
        if(sortType.equalsIgnoreCase("ID")) {
            Collections.sort(cards, Card.IdComparator);
        } else if(sortType.equalsIgnoreCase("NAME")) {
            Collections.sort(cards ,Card.NameComparator);
        }
        return cards;
    }

    public String getNameList(){
        return nameList;
    }

    public void setNameList(String newName){
        this.nameList = newName;
    }

    public long getId() {
        return id;
    }


    public String toString(){
        return "id : "+this.id+"name : "+this.nameList;
    }



}
