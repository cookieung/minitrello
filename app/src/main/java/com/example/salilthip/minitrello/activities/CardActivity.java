package com.example.salilthip.minitrello.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.AdapterView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.example.salilthip.minitrello.R;
import com.example.salilthip.minitrello.adapters.CommentAdapter;
import com.example.salilthip.minitrello.models.Card;
import com.example.salilthip.minitrello.models.Comment;
import com.example.salilthip.minitrello.utils.Storage;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CardActivity extends AppCompatActivity {

    private ListView commentListView;
    private TextView nameListText;
    private TextView titleView;
    private TextView descriptionView;
    private EditText commentText;

    private Button backBtn;
    private Button editBtn;
    private Button commentBtn;

    private Card card;
    private int idList, idCard;
    private List<Comment> commentLists;
    private CommentAdapter commentAdapter;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card);
        initComponents();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    protected void onStart(){
        super.onStart();
        refresh();
    }

    public void refresh() {
        Log.e("hello", "sss");
        titleView.setText("Title : " + card.getTitle());
        descriptionView.setText("Description : " + card.getDescription());

        commentLists.clear();
        for (Comment c : Storage.getInstance().getSaveList().get(idList).getAllCards().get(idCard).getCommentList()) {
            commentLists.add(c);
        }
        Collections.reverse(commentLists);
        commentAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {

        super.onResume();
        refresh();
    }

    public void initComponents() {

        idList = (int) getIntent().getSerializableExtra("idList");
        idCard = (int) getIntent().getSerializableExtra("idCard");
        card = Storage.getInstance().getSaveList().get(idList).getAllCards().get(idCard);
        commentText = (EditText) findViewById(R.id.comment_text);
        commentLists = new ArrayList<Comment>();
        commentListView = (ListView) findViewById(R.id.comment_view);
        commentAdapter = new CommentAdapter(this, R.layout.comment_cell, commentLists);
        commentListView.setAdapter(commentAdapter);

        final AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setTitle("Delete comment");

        commentListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {


                int size = Storage.getInstance().getSaveList().get(idList).getAllCards().get(idCard).getCommentList().size();
                builder1.setMessage(Storage.getInstance().getSaveList().get(idList).getAllCards().get(idCard).getCommentList().get(size - position - 1).getText());
                builder1.setCancelable(true);


                builder1.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        int size = Storage.getInstance().getSaveList().get(idList).getAllCards().get(idCard).getCommentList().size();
                        Storage.getInstance().getSaveList().get(idList).getAllCards().get(idCard).getCommentList().remove(size - position - 1);
                        onResume();
                        dialog.cancel();
                    }
                });

                builder1.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });

        nameListText = (TextView) findViewById(R.id.name_list_text_view_of_card);
        titleView = (TextView) findViewById(R.id.title_view);
        descriptionView = (TextView) findViewById(R.id.description_view);


        nameListText.setText(Storage.getInstance().getSaveList().get(idList).getNameList());
        refresh();

        editBtn = (Button) findViewById(R.id.edit_btn);
        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CardActivity.this, EditTestActivity.class);
                intent.putExtra("idList", idList);
                intent.putExtra("idCard", idCard);
                startActivity(intent);
            }
        });

        commentBtn = (Button) findViewById(R.id.comment_btn);
        commentBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (commentText.getText().toString().length() != 0) {
                    Comment comment = new Comment(commentText.getText().toString());
                    Storage.getInstance().getSaveList().get(idList).getAllCards().get(idCard).getCommentList().add(comment);
                    onResume();
                    commentText.setText("");
                }

            }
        });

        backBtn = (Button) findViewById(R.id.back_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }


}
