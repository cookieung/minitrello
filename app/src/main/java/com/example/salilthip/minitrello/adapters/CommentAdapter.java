package com.example.salilthip.minitrello.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.salilthip.minitrello.R;
import com.example.salilthip.minitrello.models.Comment;

import java.util.List;

/**
 * Created by Noot on 13/3/2559.
 */
public class CommentAdapter extends ArrayAdapter<Comment> {

    public CommentAdapter(Context context, int  resource, List<Comment> lc){
        super(context, resource ,lc);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if(v == null){
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.comment_cell,null);
        }

        TextView textView = (TextView) v.findViewById(R.id.comment);
        textView.setText(getItem(position).getText());
        return v;
    }
}
