package com.example.salilthip.minitrello.activities;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;

import com.example.salilthip.minitrello.R;
import com.example.salilthip.minitrello.adapters.CardAdapter;
import com.example.salilthip.minitrello.models.Card;
import com.example.salilthip.minitrello.models.CardList;
import com.example.salilthip.minitrello.utils.Storage;

import java.util.ArrayList;
import java.util.List;

public class CardListActivity extends AppCompatActivity {

    private TextView headNameList;
    private ImageView iconEdit;
    private EditText editNameList;
    private Button saveNewNameBtn;
    private ListView cardListView;
    private List<Card> cards;
    private Button create_card;
    private Button backToMainPageBtn;
    private CardAdapter cardAdapter;
    private CardList cardList;
    private TextView nameCardList;
    private int locationList;
    private Switch switchBtn;
    private boolean checkSort;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_list);
        initComponenets();
    }

    @Override
    protected void onStart(){
        super.onStart();
        refresh();
    }

    public void refresh(){
        cards.clear();
        String sortType;
        if(checkSort) {
            switchBtn.setText("Sort by: ID");
            sortType = "ID";
        }else{
            switchBtn.setText("Sort by: A-Z");
            sortType = "NAME";
        }
        cards.addAll(cardList.getAllCards(sortType));
        cardAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume(){
        super.onResume();
        refresh();
        headNameList.setVisibility(View.VISIBLE);
        editNameList.setVisibility(View.INVISIBLE);
        saveNewNameBtn.setVisibility(View.INVISIBLE);
        iconEdit.setVisibility(View.VISIBLE);
        nameCardList.setText("Number of card : " + cards.size());
    }


    private void initComponenets(){
        Intent intent = getIntent();
        locationList = (int) intent.getSerializableExtra("pos_cardlist");
        Log.i("hello", locationList + "");
        cardList = Storage.getInstance().getSaveList().get(locationList);
        editNameList = (EditText) findViewById(R.id.edit_new_name_list);
        headNameList = (TextView) findViewById(R.id.head_card_list_page);
        iconEdit = (ImageView) findViewById(R.id.icon_edit);
        headNameList.setText(Storage.getInstance().getSaveList().get(locationList).getNameList());
        editNameList.setVisibility(View.INVISIBLE);
        saveNewNameBtn = (Button) findViewById(R.id.save_new_name);
        saveNewNameBtn.setVisibility(View.INVISIBLE);
        headNameList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editNameList.setText(headNameList.getText());
                headNameList.setVisibility(View.INVISIBLE);
                iconEdit.setVisibility(View.INVISIBLE);
                editNameList.setVisibility(View.VISIBLE);
                saveNewNameBtn.setVisibility(View.VISIBLE);
            }
        });
        saveNewNameBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Storage.getInstance().getSaveList().get(locationList).setNameList(editNameList.getText().toString());
                headNameList.setText(Storage.getInstance().getSaveList().get(locationList).getNameList());
                onResume();
            }
        });
        nameCardList = (TextView) findViewById(R.id.number_of_card);
        cards = new ArrayList<Card>();
        cardAdapter = new CardAdapter(this, R.layout.card_cell, cards);
        cardListView = (ListView) findViewById(R.id.card_list_view);
        cardListView.setAdapter(cardAdapter);


        cardListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(CardListActivity.this, CardActivity.class);
                intent.putExtra("idCard", position);
                intent.putExtra("idList", locationList);
                startActivity(intent);
            }
        });

        final AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setTitle("Delete card");


        cardListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,int pos, long id) {
                final int posfi = pos;
                builder1.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Storage.getInstance().getSaveList().get(locationList).getAllCards().remove(posfi);
                        onResume();
                        dialog.cancel();
                    }
                });

                builder1.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

                AlertDialog alert11 = builder1.create();
                alert11.show();

                return true;
            }
        });

        create_card = (Button) findViewById(R.id.create_new_card);
        create_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CardListActivity.this, createNewCardActivity.class);
                intent.putExtra("location", locationList);
                startActivity(intent);
            }
        });
        //nameCardList.setText("Number of card : " + cardList.getAllCards().size());
        backToMainPageBtn = (Button) findViewById(R.id.back_to_main_page);
        backToMainPageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        switchBtn = (Switch) findViewById(R.id.switchBtn);
        switchBtn.setChecked(true);
        checkSort = switchBtn.isChecked();
        switchBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                checkSort = isChecked;
                onResume();
            }
        });
    }

}