package com.example.salilthip.minitrello.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.salilthip.minitrello.R;
import com.example.salilthip.minitrello.models.Card;

import java.util.List;

/**
 * Created by Salilthip on 3/2/2016.
 */
public class CardAdapter extends ArrayAdapter<Card> {
    public CardAdapter(Context context, int resource,List<Card> lt) {
        super(context, resource,lt);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if(v==null){
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.card_cell, null);
        }

        TextView title = (TextView) v.findViewById(R.id.title_list);
        TextView description = (TextView) v.findViewById(R.id.description_list);


        title.setText("Title : "+getItem(position).getTitle());
        description.setText("Description : "+getItem(position).getDescription());
        return v;
    }
}
